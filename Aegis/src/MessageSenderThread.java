import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Random;


public class MessageSenderThread implements Runnable{
	
	int destinationPort;
	int sourcePort;
	InetAddress host;
	
    
    MessageSenderThread(int sourcePort, int destinationPort, InetAddress host) 
    { 

      this.destinationPort = destinationPort;
      this.sourcePort = sourcePort;
      this.host = host;
    } 
    
   public void run()
   {
	   DatagramSocket socket = null;
   		try
   		{
   			socket = new DatagramSocket();
   			ObjectOutputStream oos = null;
   			String message = null;
   			byte [] data;
			
   		//	ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
   		//	ObjectOutputStream os = new ObjectOutputStream(outputStream);
   		//	DataPacket dataToSend = new DataPacket(destinationPort, "This is a message from " + destinationPort, -1, host);
			Random r = new Random();
			r.setSeed(System.currentTimeMillis());
			
		//	message = "Sending data";
		//	data = message.getBytes();
		//	DatagramPacket initPacket = new DatagramPacket( data, data.length, host, destinationPort );
		//	socket.send(initPacket);
		//	Thread.sleep(2000);
			
			
   			for(int i = 0; i < 10; i++)
			{
				
			//	dataToSend.incrementToken();
			//	DataPacket dataToSend = new DataPacket(destinationPort, "This is a message from " + destinationPort, 0, host);
				//os.writeObject(dataToSend);
   				
   				message = "DATA-"+i+"";
				data = message.getBytes();
				
	   			DatagramPacket packet = new DatagramPacket( data, data.length, host, destinationPort );
				socket.send(packet);
				
			//	Thread.sleep(r.nextInt(5000));
				Thread.sleep(3000);

			}
			
			message = "null";
			data = message.getBytes();
			DatagramPacket packet = new DatagramPacket( data, data.length, host, destinationPort );
			socket.send(packet);
			socket.close();
   		
   		}
   		
   		catch(Exception e)
   		{
   			System.out.println(e);
   		}
   		
   		finally
        {
           if( socket != null )
              socket.close() ;
        }
   	
   }
   

}
