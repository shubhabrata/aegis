import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

public class ApplicationOwner {
	
	private Vector <String> listOfActors;
	private Vector <String> dataSources;
	private Vector <String> dataSinks;
	private Hashtable <String, Vector <String>> adjacencyList;
	private Hashtable <String, String> listOfRuntimes;
	private Hashtable <String, String> listOfActorCodeLocations;
	private Hashtable <String,String> listOfActorRuntimePairs;
	private Hashtable <String,String> dataSourceInfo;
	private Hashtable <String,String> dataSinkInfo;
	

	
	ApplicationOwner()
	{
		adjacencyList = new Hashtable <String, Vector <String>>();
		listOfActors = new Vector<String>();
		listOfRuntimes = new Hashtable <String, String>();
		listOfActorCodeLocations = new Hashtable <String, String>();
		listOfActorRuntimePairs = new Hashtable <String, String>();
		dataSourceInfo = new Hashtable <String, String>();
		dataSinkInfo = new Hashtable <String, String>();
		dataSources = new Vector <String>();
		dataSinks = new Vector <String>();

	}
	
	/*
	 * Load the information pertaining to the different runtimes. Currently, a runtime identifier and 
	 * a port number for communication are stored.
	 */
	
	public void loadRuntimeInfo()
	{
		String fileLocation = "C:\\files\\runtimeInfo.txt";
		try
		{
			FileInputStream fstream = new FileInputStream(fileLocation);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String [] tokens;	
			
			//Read the graph from the file and create an adjacency list
			for(String readLine; (readLine = br.readLine()) != null; ) 
			{
				tokens = readLine.split(" ");
				if(!listOfRuntimes.containsKey(tokens[0]))
					listOfRuntimes.put(tokens[0], tokens[1]);
		    }
			
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/*
	 * Read the information of the data sources including their name and contact information
	 */
	public void loadDataSourcesInfo()
	{
		String fileLocation = "C:\\files\\dataSources.txt";
		try
		{
			FileInputStream fstream = new FileInputStream(fileLocation);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String [] tokens;	
			
			//Read the graph from the file and create an adjacency list
			for(String readLine; (readLine = br.readLine()) != null; ) 
			{
				tokens = readLine.split(" ");
				if(!dataSourceInfo.containsKey(tokens[0]))
					dataSourceInfo.put(tokens[0], tokens[1]);
		    }
			
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/*
	 * Read the information of the data sinks including their name and contact information
	 */
	public void loadDataSinksInfo()
	{
		String fileLocation = "C:\\files\\dataSinks.txt";
		try
		{
			FileInputStream fstream = new FileInputStream(fileLocation);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String [] tokens;	
			
			//Read the graph from the file and create an adjacency list
			for(String readLine; (readLine = br.readLine()) != null; ) 
			{
				tokens = readLine.split(" ");
				if(!dataSinkInfo.containsKey(tokens[0]))
					dataSinkInfo.put(tokens[0], tokens[1]);
		    }
			
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/*
	 * Retrieve the code locations of the actors
	 */
	public void getActorCodeLocations()
	{
		String fileLocation = "C:\\files\\actorCodeLocations.txt";
		try
		{
			FileInputStream fstream = new FileInputStream(fileLocation);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String [] tokens;	
			
			//Read the graph from the file and create an adjacency list
			for(String readLine; (readLine = br.readLine()) != null; ) 
			{
				tokens = readLine.split("@");
				
				if(!listOfActorCodeLocations.containsKey(tokens[0]))
					listOfActorCodeLocations.put(tokens[0], tokens[1]);
		    }
			
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/*
	 * This function allows the owner to select the nodes in the mobile cloud system where the application
	 * will be deployed. 
	 */
	public void assignActorsToRuntimes()
	{
		//Retrieve the list of available runtimes
		loadRuntimeInfo();
		
		//Retrieve the application structure using the adjacency list
		createGraphAdjacencyList();
		
		
		/*
		 * Identify the individual application nodes from the list and then assign them to the runtimes
		 * Also maintain a list of nodes assigned to runtimes
		 */
		
		//Fix this part later - The assignment of actors to runtimes
		Set s = listOfRuntimes.entrySet();
		Iterator it = s.iterator();
		
		int i = 0;
		while(it.hasNext())
		{
			Map.Entry <String, Integer> entry = (Map.Entry<String, Integer>)it.next();
			listOfActorRuntimePairs.put(listOfActors.get(i), entry.getKey());
			i++;
			if(i == listOfActors.size())
				break;
		}
		
			
	}
	
	/*
	 * This function uses the application connectivity graph to setup the connections between the different computing nodes
	 * 
	 */
	
	public void setupApplicationConnectivity()
	{
		//Use an adjacency list representation for a graph
		
		//Use the created adjacency list to setup the connectivity between the nodes
		
				
		Set s = adjacencyList.entrySet();
		Iterator i = s.iterator();
		
		String addressKey = "";
		int currentNodePort = 100;
		InetAddress currentNodeAddress = null;
		
		
		//Iterate over the list of nodes to setup the application connectivity
		while(i.hasNext())
		{
			Map.Entry <String, Vector <String>> record = (Map.Entry <String, Vector <String>>)i.next();
			
			//Get the first node
			String nodeName = record.getKey();
			addressKey = "";
			//Check if the node is a data source and get the contact information of the node
			if(nodeName.startsWith("source"))
			{
				dataSources.add(nodeName);
				addressKey = dataSourceInfo.get(nodeName);
			}
				
			else if(nodeName.equalsIgnoreCase("sink"))
			{
				dataSinks.add(nodeName);
				continue;
			}
				
			else
			{
				//If the node is an actor, then retrieve the address information of the runtime it is currently associated with
				
				String runtimeOfCurrentNode = listOfActorRuntimePairs.get(nodeName);
				addressKey = listOfRuntimes.get(runtimeOfCurrentNode);
			}
			
			//Retrieve the address and port number of the current node being examined
			
			try
			{
				int index = addressKey.indexOf("@");
				currentNodePort = Integer.parseInt(addressKey.substring(0, index));
				currentNodeAddress = InetAddress.getByName(addressKey.substring(index+1, addressKey.length()));
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			/*
			 * Once the contact information of the current node
			 * has been sorted out, start retrieving the adjacent nodes and the corresponding runtimes
			 */
								
			String listOfRuntimeAddresses = getListOfRuntimeAddressesOfAdjacentNodes(nodeName, addressKey);
			
			/*
			 * Notify the current node about its destination nodes 
			 */
			sendMessage(listOfRuntimeAddresses, currentNodeAddress, currentNodePort);
			
		}
		
	}
	
	/*
	 * Retrieving the runtimes corresponding to the adjacent nodes of a given actor node
	 */
	
	public String getListOfRuntimeAddressesOfAdjacentNodes(String currentActorNode, String currentActorAddressKey)
	{
		Vector <String> runtimes = new Vector <String> ();
		
		//Retrieve the actors adjacent to the source and their corresponding runtimes

		for(int j = 0; j < adjacencyList.get(currentActorNode).size(); j++)
		{
			String adjacentNode = adjacencyList.get(currentActorNode).get(j);
			if(adjacentNode.startsWith("sink"))
			{
				runtimes.add(adjacentNode);
				continue;
			}
			
			String runtime = listOfActorRuntimePairs.get(adjacentNode);
			System.out.println("Node " + adjacentNode + " Runtime " + runtime);
			runtimes.add(runtime);
		}
		
		
		//Retrieve the list of runtime addresses based on the runtime information from the previous list
		String listOfRuntimeAddresses = "DESTINATIONNODES";
		String runtimeAddress = "";
		
		for(int k = 0; k < runtimes.size(); k++)
		{
			if(runtimes.get(k).startsWith("sink"))
				runtimeAddress = dataSinkInfo.get(runtimes.get(k));
			else
			{
				runtimeAddress = listOfRuntimes.get(runtimes.get(k));
				
				// Send a message to the destination runtimes informing them that the current node is a data sender
				int index = runtimeAddress.indexOf("@");
				int port = 0;
				InetAddress address =  null;
				try {
					port = Integer.parseInt(runtimeAddress.substring(0, index));
					address = InetAddress.getByName(runtimeAddress.substring(index+1, runtimeAddress.length()));
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				String message = "SOURCENODES"+currentActorAddressKey;
				sendMessage(message, address, port);
			}
				
			
			listOfRuntimeAddresses = listOfRuntimeAddresses + runtimeAddress+"END";
			
			
			
		}
		
		return listOfRuntimeAddresses;
	}
	/*
	 * Generic function for sending messages to remote machines
	 */
	
	public void sendMessage(String message, InetAddress address, int port)
	{
		try
		{
			byte[] data = message.getBytes();
			
			DatagramSocket socket = new DatagramSocket();
			DatagramPacket packet = new DatagramPacket( data, data.length, address, port );
			socket.send(packet);
			socket.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/*
	 * Read the graph adjacency list from a file and populate the adjacency list
	 */
	
	public void createGraphAdjacencyList()
	{
		String fileLocation = "C:\\files\\applicationGraph.txt";
		try
		{
			FileInputStream fstream = new FileInputStream(fileLocation);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String [] tokens;	
			
			//Read the graph from the file and create an adjacency list
			for(String readLine; (readLine = br.readLine()) != null; ) 
			{
				tokens = readLine.split(" ");
				
				if(!adjacencyList.containsKey(tokens[0]))
					adjacencyList.put(tokens[0], new Vector <String>());
				
				if(!(tokens[0].contains("source")|| tokens[0].contains("sink")))
					if(!listOfActors.contains(tokens[0]))
						listOfActors.add(tokens[0]);
				
				for(int i = 1; i < tokens.length;i++)
				{
				
					adjacencyList.get(tokens[0]).add(tokens[i]);
					
					
					if(!(tokens[i].contains("source")|| tokens[i].contains("sink")))
						if(!listOfActors.contains(tokens[i]))
							listOfActors.add(tokens[i]);

				}
				
									
		        // process the line.
		    }
			
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
		
	/*
	 * This function sends a signal to the different runtimes to fetch the actor codes from the code repositories
	 */
	public void signalRuntimesToFetchActorCodes()
	{
		//Retrieve the list of the runtimes that have been assigned to the actors
		
		Set s = listOfActorRuntimePairs.entrySet();
		Iterator i = s.iterator();
		int port;
		InetAddress address = null;
		
		
		while(i.hasNext())
		{
			String message = "Fetch codesFROM";
			Map.Entry <String, String> actorRuntimePair = (Map.Entry <String, String>)i.next();
			String runtime = actorRuntimePair.getValue();
			String addressKey = listOfRuntimes.get(runtime);
			
			try
			{
				int index = addressKey.indexOf("@");
				port = Integer.parseInt(addressKey.substring(0, index));
				address = InetAddress.getByName(addressKey.substring(index+1, addressKey.length()));
				
				//Retrieve the code location of the actor
				String actorName = actorRuntimePair.getKey();
				String codeLocation = listOfActorCodeLocations.get(actorName);
				
				if(codeLocation != null)
					message += codeLocation.trim();
				
				sendMessage(message, address, port);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}
		
	}
	
	/*
	 * Notify the data sources to start the transmission of the data
	 */
	public void signalDataSources()
	{
		String message = "Start transmission";
		int port;
		InetAddress address = null;
		
		for(int i = 0; i < dataSources.size();i++)
		{
			String dataSource = dataSources.get(i);
			String addressKey = dataSourceInfo.get(dataSource);
			int index = addressKey.indexOf("@");
			port = Integer.parseInt(addressKey.substring(0, index));
			try {
				address = InetAddress.getByName(addressKey.substring(index+1, addressKey.length()));
				sendMessage(message, address, port);
			} 
			
			catch (UnknownHostException e) {
				
				e.printStackTrace();
			}
		}
		
	}
	
	public static void main(String args[])
	{
		ApplicationOwner appOwner = new ApplicationOwner();
		
		appOwner.loadRuntimeInfo();
		
		appOwner.getActorCodeLocations();
		appOwner.loadDataSourcesInfo();
		appOwner.loadDataSinksInfo();
		
		appOwner.assignActorsToRuntimes();
		appOwner.setupApplicationConnectivity();
		
		appOwner.signalRuntimesToFetchActorCodes();
		appOwner.signalDataSources();
	}
}
