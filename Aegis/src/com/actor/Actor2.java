package com.actor;

public class Actor2 {
	
	private boolean actorBusy; 
	private String processedToken;

	public Actor2()
	{
		this.actorBusy = true;
		this.processedToken = null;
	}
	
	public String processToken(String tokenValue)
	{
		synchronized(this)
		{
			processedToken = tokenValue;
			processTokenFromRuntime(tokenValue);
			this.notify();
			return this.processedToken;
		}
		
	}
	
	/*
	 * This function is used by the runtime to check the status of an actor i.e. if that actor is ready to process a request or not
	 */
	public boolean checkActorStatus()
	{
		return actorBusy;
	}
	
	public int display()
	{
		System.out.println("Actor called from runtime");
		return 1;
	}
	
	/*
	 * This function signals to the main actor thread that the runtime is ready to send the token
	 */
	public void processTokenFromRuntime(String value)
	{
		synchronized(this)
		{
			System.out.println("Ready to start processing");
			int token = Integer.parseInt(value) + 1;
			//this.processedToken = value + " added";
			this.processedToken = token + "";
			this.notify();
		}
	}
	
	
	public static void main(String[] args) 
  	{
		

		
  	}

  	
	
}




