package com.coordinator;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import com.dataPacket.*;


public class RuntimeCoordinator {
	
	InetAddress address;
	int port;
	private DatagramSocket coordinatorSocket;
	public final int PACKETSIZE = 1024 ;
	private Hashtable <String, String> listOfRuntimes;
	private Hashtable <String, String> listOfActorCodeLocations;
	private Hashtable <String, String> actorRuntimePairs;
	private Hashtable <String,String> dataSinkInfo;
	private Hashtable <String, String> replacedRuntimeList;
	private Vector <String> failedNodes;
	private Vector <String> listOfNodesReportingFailure;
	private InetAddress appOwnerAddress;
	private int appOwnerPort;
	
	RuntimeCoordinator(InetAddress address, int port)
	{
		this.address = address;
		this.port = port;
		listOfRuntimes = new Hashtable <String, String>();
		listOfActorCodeLocations = new Hashtable <String, String>();
		listOfNodesReportingFailure = new Vector <String> ();
		failedNodes = new Vector <String> ();
		dataSinkInfo = new Hashtable <String, String>();
		replacedRuntimeList = new Hashtable <String,String>();
		try
		{
			coordinatorSocket = new DatagramSocket(port);
			appOwnerAddress = InetAddress.getByName("127.0.0.1");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		appOwnerPort = 3500;
	}
	
	public void receiveData()
	{
		System.out.println("Coordinator ready @ " + port);
		
		byte[] recvBuf = new byte[5000];
		DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
		
		while(true)
		{
			try {
				coordinatorSocket.receive(packet);
				
				ByteArrayInputStream byteStream = new ByteArrayInputStream(recvBuf);
				ObjectInputStream is = new ObjectInputStream(new BufferedInputStream(byteStream));
	  			DataPacket dataPacket = (DataPacket)is.readObject();
	  			
	  			String data = dataPacket.data.trim();
	  			String message = dataPacket.message.trim();
	  			String destAddress = "";
	  			
	  			if(message.equalsIgnoreCase("ActorRuntimePairs"))
	  			{
	  				this.actorRuntimePairs = (Hashtable <String, String>)dataPacket.additionalData;
	  				System.err.println(this.actorRuntimePairs.toString());
	  			}
	  			
	  			if(message.equalsIgnoreCase("FAILEDNODE"))
	  			{
	  				String replacedRuntimeName;
	  				System.err.println("Node " + data + " has failed ");
	  				//Add the name of the node reporting the error
	  				listOfNodesReportingFailure.add(dataPacket.senderName.trim());
	  				
	  				if(!failedNodes.contains(data))
	  				{
	  					failedNodes.add(data);
		  				
		  				String address = packet.getAddress().toString();
		  				String addressKey [] = address.split("/");
		  				InetAddress reporterNodeAddress = InetAddress.getByName(addressKey[1]);
		  				int reporterNodePort = packet.getPort();
		  				
		  				replacedRuntimeName = handleAndRepairFailure(data.trim(), reporterNodeAddress, reporterNodePort);
		  				replacedRuntimeList.put(data.trim(), replacedRuntimeName);
		  				//failedNodes.remove(data);

	  				}
	  					  				
	  				if(dataPacket.senderName.equalsIgnoreCase("sink"))
	  					destAddress = dataSinkInfo.get(dataPacket.senderName);
	  				else
	  					destAddress = listOfRuntimes.get(dataPacket.senderName);
	  					
	  				//Send information about the new runtime to the reporting nodes for them to update their buffer
	  				informNodesAboutNewRuntime(destAddress, replacedRuntimeList.get(data.trim()));
	  					
	  			}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	/*
	 * Load the information pertaining to the different runtimes. Currently, a runtime identifier and 
	 * a port number for communication are stored.
	 */
	
	public void loadRuntimeInfo()
	{
		String fileLocation = "C:\\files\\runtimeInfo.txt";
		try
		{
			FileInputStream fstream = new FileInputStream(fileLocation);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String [] tokens;	
			
			//Read the graph from the file and create an adjacency list
			for(String readLine; (readLine = br.readLine()) != null; ) 
			{
				tokens = readLine.split(" ");
				if(!listOfRuntimes.containsKey(tokens[0]))
					listOfRuntimes.put(tokens[0], tokens[1]);
		    }
			
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/*
	 * Retrieve the code locations of the actors
	 */
	public void getActorCodeLocations()
	{
		String fileLocation = "C:\\files\\actorCodeLocations.txt";
		try
		{
			FileInputStream fstream = new FileInputStream(fileLocation);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String [] tokens;	
			
			//Read the graph from the file and create an adjacency list
			for(String readLine; (readLine = br.readLine()) != null; ) 
			{
				tokens = readLine.split("@");
				
				if(!listOfActorCodeLocations.containsKey(tokens[0]))
					listOfActorCodeLocations.put(tokens[0], tokens[1]);
		    }
			
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/*
	 * This function will be used by the coordinator to handle and repair the failure of a node
	 */
	public String handleAndRepairFailure(String failedNode, InetAddress reporterNodeAddress, int reporterNodePort)
	{
		
		//Step 1 - Select a free runtime for the new actor
		String replacementRuntimeInfo = getReplacementRuntimeAddress(failedNode);
		String info[] = replacementRuntimeInfo.split("AND");
		String replacementRuntimeName = info[0];
		String replacementRuntimeAddress = info[1];

		
		if(replacementRuntimeAddress.equals("empty"))
			return "";
		
		//Step 2 - Identify the actor running in the failed node
		String actorToExecute = null;
		Set s = this.actorRuntimePairs.entrySet();
		Iterator it = s.iterator();
		
		while(it.hasNext())
		{
			Map.Entry <String, String> map = (Map.Entry <String, String>) it.next();
			if(map.getValue().equals(failedNode))
			{
				actorToExecute = map.getKey();
				break;
			}
		}
		
			
		//Step 3 - Inform the application owner about the change in the actor-runtime pairing and update this information in the coordinator as well
		
		sendMessage(getDataPacket("coordinator", actorToExecute, replacementRuntimeName), appOwnerAddress, appOwnerPort);
		
		System.err.println("replace " + replacementRuntimeName);
		
		actorRuntimePairs.remove(actorToExecute);
		actorRuntimePairs.put(actorToExecute, replacementRuntimeName);	
		listOfRuntimes.remove(failedNode);
		
		//Step 4 - Alternate Inform the runtime issuing the fault detection request to perform the network reconnection
		DataPacket packet = getDataPacket("coordinator", listOfRuntimes.get(replacementRuntimeName), "InitReconnect");
		packet.additionalData = replacementRuntimeName;
		sendMessage(packet, reporterNodeAddress, reporterNodePort);
		
		//Step 4 - Report the new runtime name to all the nodes reporting the error
		
		//informNodesAboutNewRuntime(replacementRuntimeName);
		//Step 5 - Initiate the network reconnection process
		
//		sendMessage(getDataPacket("coordinator", actorToExecute, "Initiate reconnection"), appOwnerAddress, appOwnerPort);
		return replacementRuntimeName;
	}
	
	/*
	 * This function informs the nodes reporting a failure about the new runtime
	 */
	public void informNodesAboutNewRuntime(String destNodeAddress, String replacementRuntimeName)
	{
		String addressKey = "";
		InetAddress destAddress = null;
		int destPort = 0;
		String [] addrInfo = destNodeAddress.split("@");
			
		try {
			destAddress = InetAddress.getByName(addrInfo[1]);
			destPort = Integer.parseInt(addrInfo[0]);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();}
			
		sendMessage(getDataPacket("coordinator", replacementRuntimeName, "New runtime"), destAddress, destPort);
		
	}
	/*
	 * Generic function for constructing a data packet
	 */
	public DataPacket getDataPacket(String senderName, String data, String message)
	{
		
		DataPacket packet = new DataPacket();
		packet.senderName = senderName;
		packet.data = data;
		packet.message = message;
		return packet;
	}
	
	/*
	 * Generic function for sending messages to remote machines
	 */
	
	public void sendMessage(DataPacket dataPacket, InetAddress address, int port)
	{
		try
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.flush();
			oos.writeObject(dataPacket);
			byte[] data = baos.toByteArray();
			
			
			DatagramSocket socket = new DatagramSocket();
			DatagramPacket packet = new DatagramPacket( data, data.length, address, port );
			socket.send(packet);
			socket.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/*
	 * Retrieve a free runtime to run the new actor
	 */
	
	public String getReplacementRuntimeAddress(String failedNode)
	{
		String freeRuntimeInfo = "empty";
		//Iterate over the list of runtimes and check if they are associated with an actor or have failed
		Set s = this.listOfRuntimes.entrySet();
		Iterator it = s.iterator();
		
		while(it.hasNext())
		{
			Map.Entry <String, String> map = (Map.Entry <String, String>) it.next();
			String runtimeName = map.getKey();
			
			if(!actorRuntimePairs.containsValue(runtimeName) && !runtimeName.equals(failedNode))
			{
				freeRuntimeInfo = map.getKey()+"AND"+map.getValue();
				break;
			}
		}
		
		return freeRuntimeInfo;
	}
	
	/*
	 * Read the information of the data sinks including their name and contact information
	 */
	public void loadDataSinksInfo()
	{
		String fileLocation = "C:\\files\\dataSinks.txt";
		try
		{
			FileInputStream fstream = new FileInputStream(fileLocation);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String [] tokens;	
			
			//Read the graph from the file and create an adjacency list
			for(String readLine; (readLine = br.readLine()) != null; ) 
			{
				tokens = readLine.split(" ");
				if(!dataSinkInfo.containsKey(tokens[0]))
					dataSinkInfo.put(tokens[0], tokens[1]);
		    }
			
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String args[])
	{
		try
		{
			RuntimeCoordinator coordinator = new RuntimeCoordinator(InetAddress.getByName("localhost"), 7000);
					
			coordinator.loadRuntimeInfo();
			coordinator.getActorCodeLocations();
			coordinator.loadDataSinksInfo();
			coordinator.receiveData();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}

