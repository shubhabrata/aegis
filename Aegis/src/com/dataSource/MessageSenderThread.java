package com.dataSource;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Random;
import com.dataPacket.*;

public class MessageSenderThread implements Runnable{
	
	int destinationPort;
	int sourcePort;
	InetAddress host;
	
    
    MessageSenderThread(int sourcePort, int destinationPort, InetAddress host) 
    { 

      this.destinationPort = destinationPort;
      this.sourcePort = sourcePort;
      this.host = host;
    } 
    
   public void run()
   {
	   DatagramSocket socket = null;
   		try
   		{
   			socket = new DatagramSocket();
   			byte [] data;
			
   			Random r = new Random();
			r.setSeed(System.currentTimeMillis());
			
			for(int i = 0; i < 100; i++)
			{
				
				DataPacket dataPacket = new DataPacket();
				dataPacket.data = i+"";
				dataPacket.message = "DATA-";
				dataPacket.senderName = "source";
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ObjectOutputStream oos = new ObjectOutputStream(baos);
				oos.flush();
				oos.writeObject(dataPacket);
				data = baos.toByteArray();
				
				
				
				
   			//	message = "DATA-"+i+"";
			//	data = message.getBytes();
				
	   			DatagramPacket packet = new DatagramPacket( data, data.length, host, destinationPort );
				socket.send(packet);
				
				Thread.sleep(5000);

			}
			
			
			socket.close();
   		
   		}
   		
   		catch(Exception e)
   		{
   			System.out.println(e);
   		}
   		
   		finally
        {
           if( socket != null )
              socket.close() ;
        }
   	
   }
   

}
