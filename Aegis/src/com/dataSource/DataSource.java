package com.dataSource;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import com.dataPacket.*;

public class DataSource {
	
	private int sourcePort;
	public DatagramSocket dataSourceSocket;
	public Vector <String>  destinationPorts = new Vector <String>();
	private final int PACKETSIZE = 5000 ;
	public Thread dataThread;
	public Hashtable <String, Vector<String>> applicationGraph;
	public String dataSourceName;
	
	DataSource(String args[])
	{
		this.sourcePort = Integer.parseInt(args[0]);
		applicationGraph = new Hashtable <String, Vector<String>> ();

		try
		{
			dataSourceSocket = new DatagramSocket(sourcePort);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void waitForMessageFromOwner()
	{
		try
		{
			byte[] recvBuf = new byte[PACKETSIZE];
			DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
			
			System.out.println("Data source ready @ " + sourcePort);
			while(true)
			{
				
				dataSourceSocket.receive(packet);
				
				//Receive and deserialize the packet
	  			ByteArrayInputStream byteStream = new ByteArrayInputStream(recvBuf);
				ObjectInputStream is = new ObjectInputStream(new BufferedInputStream(byteStream));
	  			DataPacket dataPacket = (DataPacket)is.readObject();
	  			
	  			String data = dataPacket.data.trim();
	  			String message = dataPacket.message.trim();
	  			
	  			
	  			
	  			if(message.startsWith("DESTINATIONNODES"))
	  			{
	  				System.out.println(data);
	  				addDestinationNodes(data);
	  			}
	  				
	  			
	  			if(message.equalsIgnoreCase("Owner"))
	  				System.out.println("Received message from owner");
	  			
	  			if(message.equalsIgnoreCase("Start transmission"))
	  				dataThread.start();
	  			
	  			if(message.equalsIgnoreCase("Application graph"))
	  				applicationGraph.putAll((Hashtable)dataPacket.additionalData);

	  		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	
	/*
	 * Add the destination ports where this source is supposed to send data
	 */
	public void addDestinationNodes(String destinationNodes)
	{
		String [] destinationNodesInfo = destinationNodes.split("END");
		for(int i = 0; i < destinationNodesInfo.length; i++)
		{
			if(destinationNodesInfo[i].contains("@"))
				destinationPorts.add(destinationNodesInfo[i]);
		}
	}
	
	
	
	public static void main(String args[])
	{
		DataSource ds = new DataSource(args);
		ds.dataSourceName = "source";
		ds.dataThread = new Thread(new DataSenderThread(ds));
		ds.waitForMessageFromOwner();
		
		
	}
}

class DataSenderThread implements Runnable
{
	DataSource ds;
	DataSenderThread(DataSource ds)
	{
		this.ds = ds;
	}
	
	public void run()
	{
		byte [] data;
	
		try
		{
			for(int i = 0; i < 500; i++)
			{
				DataPacket dataPacket = new DataPacket();
				dataPacket.data = i+"";
				dataPacket.message = "DATA-";
				dataPacket.senderName = "source";
				dataPacket.sequenceNumber = i;
				dataPacket.destinationActors.addAll(ds.applicationGraph.get(ds.dataSourceName));
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ObjectOutputStream oos;
				
				oos = new ObjectOutputStream(baos);
				oos.flush();
				oos.writeObject(dataPacket);
				
				data = baos.toByteArray();
			
				for (String destPort: ds.destinationPorts)
				{
					int destinationPort = Integer.parseInt((destPort.split("@"))[0]);
					InetAddress host = null;
					host = InetAddress.getByName((destPort.split("@"))[1]);
				
					DatagramPacket packet = new DatagramPacket( data, data.length, host, destinationPort );
					ds.dataSourceSocket.send(packet);
				}
				Thread.sleep(3000);

			}
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
	}
}



