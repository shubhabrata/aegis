package com.dataPacket;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.Vector;

public class DataPacket implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int sequenceNumber;
	public String senderName;
	public InetAddress address;
	public int port;
	public String data;
	public String message;
	public Object additionalData;
	public Vector <String> destinationActors = new Vector <String>();
	

}
