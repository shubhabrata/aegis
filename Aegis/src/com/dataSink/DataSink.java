package com.dataSink;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import com.dataPacket.*;

public class DataSink {
	
	InetAddress address;
	int port;
	private DatagramSocket dataSinkSocket;
	private Vector <String> receivedPackets;
	private final int PACKETSIZE = 5000 ;

	public Vector <String> failedNodes;
	private Vector <String> destinationNodeInfo;
	private Vector<String> activeDataSources;
	public Vector <String> sourceRuntimes;
	public Hashtable <String, String> runtimeInfo;
	public Hashtable <String, Vector<String>> dataStreams;
	public boolean checkModeOn;
	public String name;

	DataSink(InetAddress address, int port)
	{
		this.address = address;
		this.port = port;
		receivedPackets = new Vector <String>();

		failedNodes = new Vector <String> ();
		destinationNodeInfo = new Vector <String>();
		activeDataSources = new Vector <String>();
		sourceRuntimes = new Vector <String> ();
		runtimeInfo = new Hashtable <String, String> ();
		dataStreams = new Hashtable <String, Vector <String>>();
		checkModeOn = false;

		try
		{
			dataSinkSocket = new DatagramSocket(port);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	public void receiveData()
	{
		System.out.println("Sink ready @ " + port);
		
		while(true)
	  	{
			byte[] recvBuf = new byte[PACKETSIZE];
			DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
	  		try
	  		{
	  			
	  			dataSinkSocket.receive(packet);
	  			
	  			ByteArrayInputStream byteStream = new ByteArrayInputStream(recvBuf);
				ObjectInputStream is = new ObjectInputStream(new BufferedInputStream(byteStream));
	  			DataPacket dataPacket = (DataPacket)is.readObject();
	  			String data = dataPacket.data.trim();
	  			String message = dataPacket.message.trim();

	  			if(message.startsWith("DATA-"))
	  			{
	  				String address = packet.getAddress()+"";
	  				String tokens[] = address.split("/");
	  				
	  				addTokenToDataStream(dataPacket);

	  				if(!sourceRuntimes.contains(dataPacket.senderName.trim()))
	  					sourceRuntimes.add(dataPacket.senderName.trim());
	  				
	  				if(!runtimeInfo.containsKey(dataPacket.senderName.trim()))
	  					runtimeInfo.put(dataPacket.senderName.trim(), packet.getPort()+"@"+tokens[1]);
	  				
  				
	  				if(!receivedPackets.contains(data))
	  				{
	  					receivedPackets.add(data);
	  					System.out.println(data + " From " + dataPacket.senderName);
	  				}
	  				
	  			}
	  			
	  			if(message.startsWith("DESTINATIONNODES"))
	  			{
	  				System.out.println(data);
	  				addDestinationNodes(data);
	  			}
	  			
	  			if(message.startsWith("SOURCENODES"))
	  			{
	  				System.err.println(data);
	  				addSourceNode(data);
	  			}
	  			
	  			if(message.startsWith("InitReconnect"))
	  				initiateReconnectionProcess(data);
	  			
	  			if(message.startsWith("New runtime"))
	  				addNewSourceRuntimeInformation(data);
	  		}
	  		catch(Exception e)
	  		{
	  			e.printStackTrace();
	  		}
	  	}
	}
	
	
	/*
	 * Function to construct the data stream from the tokens received by the runtime
	 */
	
	public void addTokenToDataStream(DataPacket packet)
	{
		System.err.println(packet.senderName + " " + packet.sequenceNumber + " " + packet.data);
		String sender = packet.senderName.trim();
		
		if(!dataStreams.containsKey(sender))
			dataStreams.put(sender, new Vector <String>());
		
		if(!dataStreams.get(sender).contains(packet.data))
			dataStreams.get(sender).add(packet.sequenceNumber, packet.data);
	}
	
	/*
	 * This function performs the task of adding a new source runtime information
	 */
	
	public void addNewSourceRuntimeInformation(String newRuntimeName)
	{
		//1. Get a copy of a vector corresponding to an active data source
		String sourceName = sourceRuntimes.get(0);
		Vector <String> bufferToCopy = new Vector <String>();
		bufferToCopy.addAll(dataStreams.get(sourceName));
		
		//2. Copy this buffer to the entry of the new runtime
		
		dataStreams.put(newRuntimeName, bufferToCopy);
		System.err.println(dataStreams.toString());
	}
	/*
	 * This function performs the task of sending the new runtime information
	 * to one of its source nodes for rewiring the network connections 
	 */
	
	public void initiateReconnectionProcess(String newRuntimeAddress)
	{
		String runtimeToReplicateConnections="";
		InetAddress runtimeAddress = null;
		//Step 1 - search for a suitable source node
		for(String currentRuntime: sourceRuntimes)
		{
			
			if(!failedNodes.contains(currentRuntime))
			{
				runtimeToReplicateConnections = currentRuntime;
				break;
			}
				
		}
		
		String addressKey = runtimeInfo.get(runtimeToReplicateConnections);
		String [] addr = addressKey.split("@");
		try {
			runtimeAddress = InetAddress.getByName(addr[1]);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int runtimePort = Integer.parseInt(addr[0]);
		
		//Send the reconnection information to the new runtime
		System.err.println("Sending to " + runtimePort);
		sendMessage(getDataPacket("sink", newRuntimeAddress, "Reconnect"), runtimeAddress, runtimePort);
		
	}
	
	
	/*
	 * Add the information of the nodes that send data to this particular runtime
	 */
	public void addSourceNode(String sourceNode)
	{
		String [] sourceNodesInfo = sourceNode.split("END");
		for(int i = 0; i < sourceNodesInfo.length; i++)
		{
			if(!activeDataSources.contains(sourceNodesInfo[i]))
				activeDataSources.add(sourceNodesInfo[i]);
		}
		
		for(String dataSource: activeDataSources)
			System.out.println("Receiving data from " + dataSource);
	}
	
	/*
	 * Generic function for constructing a data packet
	 */
	public DataPacket getDataPacket(String senderName, String data, String message)
	{
		
		DataPacket packet = new DataPacket();
		packet.senderName = senderName;
		packet.data = data;
		packet.message = message;
		return packet;
	}
	
	/*
	 * Add the destination ports where this source is supposed to send data
	 */
	public void addDestinationNodes(String destinationNodes)
	{
		String [] destinationNodesInfo = destinationNodes.split("END");
				
		for(String destNodeInfo:destinationNodesInfo)
		{
			if(destNodeInfo.contains("@"))
				destinationNodeInfo.add(destNodeInfo);
		}
	}
	
	/*
	 * Generic function for sending messages to remote machines
	 */
	
	public void sendMessage(DataPacket dataPacket, InetAddress address, int port)
	{
		try
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.flush();
			oos.writeObject(dataPacket);
			byte[] data = baos.toByteArray();
			
			DatagramPacket packet = new DatagramPacket( data, data.length, address, port );
			dataSinkSocket.send(packet);
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	
	public static void main(String args[])
	{
		try
		{
			DataSink ds = new DataSink(InetAddress.getByName("127.0.0.1"), 2000);
			ds.name = "sink";
			new Thread(new FailureDetectionThread(ds)).start();

			ds.receiveData();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	

}

class FailureDetectionThread implements Runnable
{
	private DataSink sink;

	FailureDetectionThread(DataSink sink)
	{
		this.sink = sink;
	}
	
	public void run()
	{
		while(true)
		{
			try {
				Thread.sleep(700);
				if(!sink.checkModeOn)
					checkForFailures();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void checkForFailures()
	{
		Set s = sink.dataStreams.entrySet();
		Iterator it = s.iterator();
		String lastTokenFromFailedNode = null;

		Vector <String> vectorToCompare = new Vector <String>();
		Vector <String> listOfFailedNodes = new Vector <String> ();

		//Retrieve the first vector stream
		String senderName = "";
		if(it.hasNext())
		{
			Map.Entry <String, Vector <String>> map = (Map.Entry <String, Vector <String>>) it.next();
			senderName = map.getKey();
			vectorToCompare = map.getValue();
		}
		
		while(it.hasNext())
		{
			Map.Entry <String, Vector <String>> map = (Map.Entry <String, Vector <String>>) it.next();
			Vector <String> currentVector = map.getValue();
			
			if(currentVector.equals(vectorToCompare))
				continue;
			else
			{
				if(currentVector.size() < vectorToCompare.size())
				{
					//Monitor the data stream for a small period of time to avoid failure detection due to network delay
					try {
						Thread.sleep(3000);
						if(currentVector.size() == vectorToCompare.size())
							continue;
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				senderName = map.getKey();
				
				System.err.println(sink.dataStreams.toString());
				System.err.println(senderName + " has stopped sending data");
				sink.checkModeOn = true;
				//Take note of the last received token of the failed node
				
				//Make note of the node to be removed from the tables
				listOfFailedNodes.add(senderName);
				
				sink.failedNodes.add(senderName);
				//initiate the recovery process using coordinator
				recoverFromNodeFailureUsingCoordinator(senderName, lastTokenFromFailedNode);
				
				//initiate the recovery process without using coordinator
				//recoverFromNodeFailure(map.getKey(), lastTokenFromFailedNode);
				
			}
		}
		
		for(String failedNode:listOfFailedNodes)
		{

			sink.dataStreams.remove(failedNode);
			sink.sourceRuntimes.remove(failedNode);

		}
		
		sink.checkModeOn = false;
		
	}
	
	public void recoverFromNodeFailureUsingCoordinator(String failedNode, String lastTokenFromFailedNode)
	{
		//Inform the coordinator about the failed node
		
		try {
		//	runtime.sendMessage("FAILEDNODE"+failedNode, InetAddress.getByName("127.0.0.1"), 8000);
			
			sink.sendMessage(sink.getDataPacket(sink.name, failedNode, "FAILEDNODE"), InetAddress.getByName("127.0.0.1"), 7000);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}

