package com.runtime;

public class ActorInformation {

	private String actorName;
	private Class classToExecute;
	private Object actorInstance;
	private String actorCodeLocation;
	
	ActorInformation(String actorName, Class classToExecute, Object actorInstance, String actorCodeLocation)
	{
		this.actorName = actorName;
		this.classToExecute = classToExecute;
		this.actorInstance = actorInstance;
		this.actorCodeLocation = actorCodeLocation;
	}
	
	public Class getClassToExecute()
	{
		return this.classToExecute;
	}
	
	public Object getActorInstance()
	{
		return this.actorInstance;
	}
	
	public String getActorCodeLocation()
	{
		return this.actorCodeLocation;
	}
}
