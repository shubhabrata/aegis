package com.runtime;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.JarURLConnection;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.jar.Attributes;
import com.dataPacket.*;

import com.coordinator.RuntimeCoordinator;

public class Runtime extends ClassLoader{
	
	private InetAddress address;
	private int port;
	private DatagramSocket runtimeSocket;
	public String runtimeName;
	public String runtimeAddressInformation;
	
	public Vector<String> activeDataSources;
	private Vector <String> destinationNodeInfo;
	public Vector <String> sourceRuntimes;
	public Hashtable <String, String> runtimeInfo;
	public Vector <String> listOfActors;
	public Hashtable <String, ActorInformation> actorInfoList;
	public Hashtable <String, Vector<String>> applicationGraph;

	
	public Hashtable <String, BlockingQueue<DataPacket>> listOfInputBuffers;
	public Hashtable <String, BlockingQueue<DataPacket>> listOfOutputBuffers;

	
	public BlockingQueue <DataPacket> inputBuffer;
	public BlockingQueue <DataPacket> outputBuffer;
	public Vector <String> failedNodes;
	public Vector <String> processedValues;
	public Hashtable <String, Vector<String>> dataStreams;
	private Hashtable <String, Thread> listOfActorThreads;
	private Hashtable <String, Thread> listOfActorDataThreads;
	public Class classToExecute;
	public Object actorInstance;

	//public boolean inputBufferEmpty = true;
	//public boolean outputBufferEmpty = true;
	public Hashtable <String, Boolean> inputBufferState;
	public Hashtable <String, Boolean> outputBufferState;
	public boolean checkModeOn;

	public static int portNumber = 8000;
	private final int PACKETSIZE = 5000 ;
	
	Runtime(String address, int port)
	{
		runtimeAddressInformation = port + "@" + address;
		try {
			this.address = InetAddress.getByName(address);
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		this.port = port;
		
		destinationNodeInfo = new Vector <String>();
		activeDataSources = new Vector <String>();
		dataStreams = new Hashtable <String, Vector <String>>();
		
		inputBuffer = new LinkedBlockingQueue<DataPacket>();
		outputBuffer = new LinkedBlockingQueue<DataPacket>();

		listOfActors = new Vector <String> ();
		listOfActorThreads = new Hashtable <String, Thread> ();
		listOfActorDataThreads = new Hashtable <String, Thread> ();
		actorInfoList = new Hashtable <String, ActorInformation> ();
		
		listOfInputBuffers = new Hashtable <String, BlockingQueue<DataPacket>> ();
		listOfOutputBuffers = new Hashtable <String, BlockingQueue<DataPacket>> ();

		inputBufferState = new Hashtable <String, Boolean> ();
		outputBufferState = new Hashtable <String, Boolean> ();

		
		runtimeInfo = new Hashtable <String, String> ();
		failedNodes = new Vector <String> ();
		sourceRuntimes = new Vector <String> ();
		processedValues = new Vector <String> ();
		applicationGraph = new Hashtable <String, Vector<String>> ();
		checkModeOn = false;
		
		try
		{
			SocketAddress addr = new InetSocketAddress(address, port);
			runtimeSocket = new DatagramSocket(null);
			runtimeSocket.bind(addr);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	

	public void receiveMessages()
	{
	//	String receivedData = null;
		byte[] recvBuf = new byte[PACKETSIZE];
		DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
	  	
		System.out.println("Runtime ready @ " + port);
	  	while(true)
	  	{
	  		try
	  		{
	  			runtimeSocket.receive(packet);
	  			
	  			//Receive and deserialize the packet
	  			ByteArrayInputStream byteStream = new ByteArrayInputStream(recvBuf);
				ObjectInputStream is = new ObjectInputStream(new BufferedInputStream(byteStream));
	  			DataPacket dataPacket = (DataPacket)is.readObject();
	  			String data = dataPacket.data.trim();
	  			String message = dataPacket.message.trim();
	  			
	  			if( message.startsWith("DATA-"))
	  			{
	  				System.out.println(data + " FROM " + dataPacket.senderName);
	  				
	  				String address = packet.getAddress()+"";
	  				String tokens[] = address.split("/");
	  				
	  				//Add the token to the data stream corresponding to the source
	  				
	  				addTokenToDataStream(dataPacket);
	  					  					  				
	  				if(!sourceRuntimes.contains(dataPacket.senderName.trim()))
	  					sourceRuntimes.add(dataPacket.senderName.trim());
	  				
	  				if(!runtimeInfo.containsKey(dataPacket.senderName.trim()))
	  					runtimeInfo.put(dataPacket.senderName.trim(), packet.getPort()+"@"+tokens[1]);

	  				//Retrieve the list of destination actors from the data packet
	  				Vector <String> destActors = dataPacket.destinationActors;
	  				

	  				//Check which of the destination actors are hosted in this runtime
	  				for(String actorName: destActors)
	  				{
	  					if(listOfActors.contains(actorName))
	  						handleDataStream(actorName, dataPacket, packet.getPort()+"@"+tokens[1]);
	  				}
	  				 
	  				//handleDataStream(dataPacket, packet.getPort()+"@"+tokens[1]);
	  				
	  			}
	  				
	  			if(message.startsWith("DESTINATIONNODES"))
	  			{
	  				System.out.println(data);
	  				addDestinationNodes(data);
	  			}
	  			
	  			if(message.startsWith("SOURCENODES"))
	  			{
	  				System.err.println(data);
	  				addSourceNode(data);
	  			}
	  			
	  			if(message.startsWith("Fetch codes"))
	  			{
	  				String actorName = dataPacket.additionalData.toString();
	  				listOfActors.add(actorName);
	  				
	  				//Setup an input and output buffer for this actor
	  				listOfInputBuffers.put(actorName, new LinkedBlockingQueue<DataPacket>());
	  				listOfOutputBuffers.put(actorName, new LinkedBlockingQueue<DataPacket>());
	  				
	  				//Setup the buffer states for the actor
	  				
	  				inputBufferState.put(actorName, true);
  					outputBufferState.put(actorName, true);
  					
  					//Fetch and deploy the actor code
	  				fetchAndSetupActorCode(actorName, data);
	  				  				
	  				if(!listOfActorThreads.containsKey(actorName))
	  					listOfActorThreads.put(actorName, new Thread(new ActorRuntimeInteractionThread(this, actorName)));
	  				
	  				listOfActorThreads.get(actorName).start();
	  				//Add a separate data transmission thread for each actor
	  				if(!listOfActorDataThreads.containsKey(actorName))
	  					listOfActorDataThreads.put(actorName, new Thread(new DataTransmissionThread(this, actorName)));
	  				
	  				listOfActorDataThreads.get(actorName).start();
	  				
	  				
	  			}
	  			
	  			if(message.startsWith("Failed node"))
	  				initiateRecoveryProcess(data);
	  			  			
	  			if(message.startsWith("InitReconnect"))
	  				initiateReconnectionProcess(data);
	  				  			
	  			if(message.startsWith("Reconnect"))
	  				reconnectNewRuntime(data, dataPacket.sequenceNumber);
	  				  			
	  			if(message.startsWith("Resume data"))
	  				resumeDataFlowOperations(dataPacket.sequenceNumber);
	  			
	  			if(message.startsWith("Migrate state"))
	  				migrateStateInformation(dataPacket);
	  			
	  			if(message.startsWith("New runtime"))
	  				addNewSourceRuntimeInformation(data);
	  			
	  			if(message.startsWith("Application graph"))
	  				applicationGraph.putAll((Hashtable)dataPacket.additionalData);
	  				  				
	  		}
	  		catch(Exception e)
	  		{
	  			e.printStackTrace();
	  		}
	  	}
	}
	
		
	/*
	 * Generic function for constructing a data packet
	 */
	public DataPacket getDataPacket(String senderName, String data, String message)
	{
		
		DataPacket packet = new DataPacket();
		packet.senderName = senderName;
		packet.data = data;
		packet.message = message;
		return packet;
	}
	
	/*
	 * Add the destination ports where this source is supposed to send data
	 */
	public void addDestinationNodes(String destinationNodes)
	{
		String [] destinationNodesInfo = destinationNodes.split("END");
		for(String destNodeInfo:destinationNodesInfo)
		{
			if(destNodeInfo.contains("@"))
				destinationNodeInfo.add(destNodeInfo);
		}
	}
	
	/*
	 * Add the information of the nodes that send data to this particular runtime
	 */
	public void addSourceNode(String sourceNodes)
	{
		String [] sourceNodesInfo = sourceNodes.split("END");
		
		for(String sourceNode : sourceNodesInfo)
		{
			if(!activeDataSources.contains(sourceNode))
				activeDataSources.add(sourceNode);
		}
		
	
	}
	
	/*
	 * This function performs the task of adding a new source runtime information
	 */
	
	public void addNewSourceRuntimeInformation(String newRuntimeName)
	{
		//1. Get a copy of a vector corresponding to an active data source
		String sourceName = sourceRuntimes.get(0);
		Vector <String> bufferToCopy = new Vector <String>();
		bufferToCopy.addAll(dataStreams.get(sourceName));
				
			
		//2. Copy this buffer to the entry of the new runtime
		dataStreams.put(newRuntimeName, bufferToCopy);
	}
	
	/*
	 * Fetch and execute the actor code from a code repository
	 */
	public void fetchAndSetupActorCode(String actorName, String codeLocation)
	{
		try
		{
			File file = new File(codeLocation);
			URL url = file.toURI().toURL();
	        URL[] urls = {url};
			ClassLoader classLoader = new URLClassLoader(urls);
			
			//Load the main class
			URL u = new URL("jar", "", url + "!/");
			JarURLConnection uc = (JarURLConnection)u.openConnection();
			Attributes attr = uc.getMainAttributes();
			String className = attr != null ? attr.getValue(Attributes.Name.MAIN_CLASS): null;
			classToExecute = classLoader.loadClass(className);
			
			Constructor constructor = classToExecute.getConstructor();
			actorInstance= constructor.newInstance();
			
			//Collect all the information pertaining to an actor in a class
			ActorInformation actorInfo = new ActorInformation(actorName, classToExecute, actorInstance, codeLocation);
			actorInfoList.put(actorName, actorInfo);
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/*
	 * Function to construct the data stream from the tokens received by the runtime
	 */
	
	public void addTokenToDataStream(DataPacket packet)
	{
		System.err.println(packet.senderName + " " + packet.sequenceNumber + " " + packet.data);
		String sender = packet.senderName.trim();
		
		if(!dataStreams.containsKey(sender))
			dataStreams.put(sender, new Vector <String>());
		
		if(!dataStreams.get(sender).contains(packet.data))
			dataStreams.get(sender).add(packet.sequenceNumber, packet.data);
		
	}
	
	/*
	 * Function to receive the incoming data streams
	 */
	
	public void handleDataStream(String actorName, DataPacket packet, String dataSourceId)
	{
			
		if(packet.data.equalsIgnoreCase("null"))
			return;
				
		addToBuffers(actorName, packet);
		
	}
	
	/*
	 * Function to initiate the recovery process for a failed node
	 */
	
	public void initiateRecoveryProcess(String failedNode)
	{
		//Step 1 - Find a replacement runtime in collaboration with the remaining runtimes - distributed consensus
		
		//Step 2 - Once the replacement runtime has been selected, repair the connections and setup the network
	}
	
	
	/*
	 * This function allows a new runtime to copy its current source buffers from one of its replicas.
	 * It also sends a copy of these buffers to its sources
	 */
	
	public void migrateStateInformation(DataPacket packet)
	{
		//1. Retrieve the source names and fill in the data buffers corresponding to these sources
		dataStreams = (Hashtable <String, Vector <String>>)packet.additionalData;
	}
	/*
	 * This function performs the task of sending the new runtime information
	 * to one of its source nodes for rewiring the network connections 
	 */
	
	public void initiateReconnectionProcess(String newRuntimeAddress)
	{
		String runtimeToReplicateConnections="";
		InetAddress runtimeAddress = null;
		//Step 1 - search for a suitable source node
		for(String currentRuntime: sourceRuntimes)
		{
			if(!failedNodes.contains(currentRuntime))
			{
				runtimeToReplicateConnections = currentRuntime;
				break;
			}
				
		}
		
		String addressKey = runtimeInfo.get(runtimeToReplicateConnections);
		String [] addr = addressKey.split("@");
		try {
			runtimeAddress = InetAddress.getByName(addr[1]);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int runtimePort = Integer.parseInt(addr[0]);
		
		//Make a note of the last received data from the failed node
		DataPacket packet = getDataPacket(this.runtimeName, newRuntimeAddress, "Reconnect");
		sendMessage(packet, runtimeAddress, runtimePort);
		
	}
	
	/*
	 * Reestablish the network connections for the new runtime
	 */
	
	public void reconnectNewRuntime(String destinationRuntimeAddress, int lastToken)
	{
		//System.err.println("In here");
		InetAddress destAddress = null;
		
		//1. Retrieve the address information of the destination runtime
		
		String [] addr = destinationRuntimeAddress.split("@");
		try {
			destAddress = InetAddress.getByName(addr[1]);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int destPort = Integer.parseInt(addr[0]);
		
		//2. Redeploy the actor in the destination runtime
		for(String actorName : listOfActors)
		{
			String codeLocation = actorInfoList.get(actorName).getActorCodeLocation();
			DataPacket packet = getDataPacket(runtimeName, codeLocation, "Fetch codes");
			packet.additionalData = actorName;
			sendMessage(packet, destAddress, destPort);
		}
		
		
		//3. Send the destination runtime the information about the destination nodes
		String destinationRuntimeAddresses = "";
		
		for(String destNode : destinationNodeInfo)
			destinationRuntimeAddresses += destNode+"END";
			
		sendMessage(getDataPacket(this.runtimeName, destinationRuntimeAddresses, "DESTINATIONNODES"), destAddress, destPort);
		
		//4. Send the destination runtime the information about the source nodes
		String sourceAddresses = "";
		
		for(String sourceNode : activeDataSources)
			sourceAddresses += sourceNode+"END";
		
		sendMessage(getDataPacket(this.runtimeName, sourceAddresses, "SOURCENODES"), destAddress, destPort);
		
		//5. Handle the buffer handover part here
		DataPacket packet = getDataPacket(this.runtimeName, "", "Migrate state");
		packet.additionalData = dataStreams;
		sendMessage(packet, destAddress, destPort);
		
		//6. Send a message to the destination runtime to setup and resume its data flow
		
		packet = getDataPacket(this.runtimeName, sourceAddresses, "Resume data flow");
		packet.sequenceNumber = lastToken;
		sendMessage(packet, destAddress, destPort);
		
	}
	
	/*
	 * This function is used to reconnect a new runtime to its source nodes and resuming the data
	 * flow operation
	 */
	
	public void resumeDataFlowOperations(int lastToken)
	{
		
		//1. Send a message to all the source nodes of this runtime to add it as a destination node
		InetAddress destAddr = null;
		int destPort = 0;
		for(String addressKey : activeDataSources)
		{
			String [] addr = addressKey.split("@");
			try {
				destAddr = InetAddress.getByName(addr[1]);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			destPort = Integer.parseInt(addr[0]);
			
			sendMessage(getDataPacket(runtimeName, runtimeAddressInformation+"END", "DESTINATIONNODES"), destAddr, destPort);
		}
		
	
		//2. Send a resume data flow message to all the data sources
		
		for(String addressKey: activeDataSources)
		{
			String [] addr = addressKey.split("@");
			try {
				destAddr = InetAddress.getByName(addr[1]);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			destPort = Integer.parseInt(addr[0]);
			DataPacket packet = getDataPacket(runtimeName, runtimeAddressInformation, "Repair");
			packet.sequenceNumber = lastToken;
			sendMessage(packet, destAddr, destPort);
		}
		
	}
	/*
	 * Function to forward the data to the destination nodes
	 */
	
	public void sendToDestinationNodes(DataPacket packet)
	{
		InetAddress address = null;
		int port = 0;
		for(String addressKey: destinationNodeInfo)
		{
			String [] addrInfo = addressKey.split("@");
			try {
				address = InetAddress.getByName(addrInfo[1]);
				port = Integer.parseInt(addrInfo[0]);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

			sendMessage(packet, address, port);
		}
	}
	
	/*
	 * Generic function for sending messages to remote machines
	 */
	
	public void sendMessage(DataPacket dataPacket, InetAddress address, int port)
	{
		try
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.flush();
			oos.writeObject(dataPacket);
			byte[] data = baos.toByteArray();
			
			DatagramPacket packet = new DatagramPacket( data, data.length, address, port );
			runtimeSocket.send(packet);
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void addToBuffers(String actorName, DataPacket packet)
	{
		if(packet.data.equals("null"))
			return;
			
		if(!processedValues.contains(packet.data))
		{
			processedValues.add(packet.data);
			sendToInputBuffer(actorName, packet);

		}
		
	}
	
	public void sendToInputBuffer(String actorName, DataPacket packet)
	{
		synchronized(listOfInputBuffers.get(actorName))
		{
			listOfInputBuffers.get(actorName).add(packet);

			//Send a message to the thread denoting that a new value has arrived

			inputBufferState.put(actorName, false);
			listOfInputBuffers.get(actorName).notify();
		}
	}
	
	/*
	 * This function is used to initiate the recovery from a node failure 
	 */
	
	public void informSourceNodesAboutNodeFailure(String failedNode, String lastTokenFromFailedNode)
	{
		//Send information to the other source nodes about the failed node
		InetAddress sourceNodeAddress = null;
		int sourceNodePort = 0;
		
		for(String currentNode: activeDataSources)
		{
			if(currentNode.equals(failedNode))
				continue;
			
			String [] addressKey = currentNode.split("@");
			try {
				sourceNodeAddress = InetAddress.getByName(addressKey[1]);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sourceNodePort = Integer.parseInt(addressKey[0]);
			sendMessage(getDataPacket(this.runtimeName, failedNode, "Failed node"), sourceNodeAddress, sourceNodePort);
			
		}
		
		
	}

	
	public int getBufferSize()
	{
		return inputBuffer.size();
				
	}
	
		
	public void addToOutputBuffer(String actorName, DataPacket packet)
	{
		synchronized(this.listOfOutputBuffers.get(actorName))
		{
			this.listOfOutputBuffers.get(actorName).add(packet);
		
			outputBufferState.put(actorName, false);
			this.listOfOutputBuffers.get(actorName).notify();
		}
	}
	
		
	public static void main(String args[])
	{
		try
		{
			Runtime runtime = new Runtime("127.0.0.1", Integer.parseInt(args[0]));
			runtime.runtimeName = "Runtime"+ Integer.parseInt(args[0]);
		//	new Thread(new ActorRuntimeInteractionThread(runtime)).start();
		//	new Thread(new DataTransmissionThread(runtime)).start();
			new Thread(new FailureDetectionThread(runtime)).start();
			runtime.receiveMessages();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
}


/*
 * This thread monitors the incoming data streams and checks if a failure has occurred
 */

class FailureDetectionThread implements Runnable
{
	private Runtime runtime;

	FailureDetectionThread(Runtime runtime)
	{
		this.runtime = runtime;
	}
	
	public void run()
	{
		while(true)
		{
			try {
				Thread.sleep(700);
				if(!runtime.checkModeOn)
					checkForFailures();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void checkForFailures()
	{
		Set s = runtime.dataStreams.entrySet();
		Iterator it = s.iterator();

		Vector <String> vectorToCompare = new Vector <String>();
		Vector <String> listOfFailedNodes = new Vector <String> ();

		//Retrieve the first vector stream
		String senderName = "";
		
		if(it.hasNext())
		{
			Map.Entry <String, Vector <String>> map = (Map.Entry <String, Vector <String>>) it.next();
			senderName = map.getKey();
			vectorToCompare = map.getValue();
		}
		
		while(it.hasNext())
		{
			Map.Entry <String, Vector <String>> map = (Map.Entry <String, Vector <String>>) it.next();
			Vector <String> currentVector = map.getValue();
			
			if(currentVector.equals(vectorToCompare))
				continue;
			else
			{
				if(currentVector.size() < vectorToCompare.size())
				{
					//Monitor the data stream for a small period of time to avoid failure detection due to network delay
					try {
						Thread.sleep(3000);
						if(currentVector.size() == vectorToCompare.size())
							continue;
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				senderName = map.getKey();
				
				System.err.println(runtime.dataStreams.toString());
				System.err.println(senderName + " has stopped sending data");
				runtime.checkModeOn = true;
				
				//Make note of the node to be removed from the tables
				listOfFailedNodes.add(senderName);
				
				runtime.failedNodes.add(senderName);
				//initiate the recovery process using coordinator
				recoverFromNodeFailureUsingCoordinator(senderName);
				
				//initiate the recovery process without using coordinator
				//recoverFromNodeFailure(map.getKey(), lastTokenFromFailedNode);
				
			}
		}
		
		for(String failedNode:listOfFailedNodes)
		{
			runtime.dataStreams.remove(failedNode);
			runtime.activeDataSources.remove(failedNode);
			runtime.sourceRuntimes.remove(failedNode);
		}
		
		runtime.checkModeOn = false;
	}
	
		
	public void recoverFromNodeFailureUsingCoordinator(String failedNode)
	{
		//Inform the coordinator about the failed node
		
		try {
			
			runtime.sendMessage(runtime.getDataPacket(runtime.runtimeName, failedNode, "FAILEDNODE"), InetAddress.getByName("127.0.0.1"), 7000);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void recoverFromNodeFailure(String failedNode, String lastTokenFromFailedNode)
	{
		/*
		 * Inform the other source nodes about the failure 
		 */
		
		runtime.informSourceNodesAboutNodeFailure(failedNode, lastTokenFromFailedNode);
	}
}

/*
 * This thread is used to control the interaction between the actor
 * and the runtime
 */
class ActorRuntimeInteractionThread implements Runnable
{
	private Runtime runtime;
	private Class classToExecute;
	private Object actorInstance;
	private String actorName;
	ActorRuntimeInteractionThread(Runtime runtime, String actorName)
	{
		this.runtime = runtime;
		this.actorName = actorName;
		classToExecute = runtime.actorInfoList.get(actorName).getClassToExecute();
		actorInstance = runtime.actorInfoList.get(actorName).getActorInstance();
	}

	@Override
	public void run() {
		
		String value=null;
		String tokenReceivedFromActor = null;
		int sequenceNumber = 0;
		synchronized(runtime.listOfInputBuffers.get(actorName))
		{
			while(runtime.inputBufferState.get(actorName))
			{
				try {
					runtime.listOfInputBuffers.get(actorName).wait();
					
					
					// Check with actor if it is ready for processing the token
					Method actorMethod = classToExecute.getMethod("checkActorStatus");
					
					if((boolean)actorMethod.invoke(actorInstance))
					{
						DataPacket packet = runtime.listOfInputBuffers.get(actorName).remove();
						value = packet.data;
						sequenceNumber = packet.sequenceNumber;
						
						//Initiate the token processing operation with the actor by notifying the actor
						
						Method initateActorOperation = classToExecute.getMethod("processToken", new Class[]{String.class});
						tokenReceivedFromActor = initateActorOperation.invoke(actorInstance, new String(value))+"";
					}
					
					DataPacket packetToSend= runtime.getDataPacket(runtime.runtimeName, tokenReceivedFromActor, "DATA-");
					packetToSend.sequenceNumber = sequenceNumber;
					
					// Retrieve the destination actor information from the adjacency list and add it to the data packet
					packetToSend.destinationActors.addAll(runtime.applicationGraph.get(actorName));
					
					runtime.addToOutputBuffer(actorName,packetToSend);
					System.out.println("The value is " + tokenReceivedFromActor);
					
					runtime.inputBufferState.put(actorName, true);
				} 
				
				catch (InterruptedException e) {
					e.printStackTrace();
				}
				catch (NoSuchMethodException e) {
					e.printStackTrace();
				}
				catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				}
			}
			
			return;
		}
	}
	
}

/*
 * This thread monitors the output buffer and sends values from the output buffer to 
 * the destination nodes
 */
class DataTransmissionThread implements Runnable
{
	Runtime runtime;
	String actorName;
	DataTransmissionThread(Runtime runtime, String actorName)
	{
		this.runtime = runtime;
		this.actorName = actorName;
	}
	
	public void run()
	{
		synchronized(runtime.listOfOutputBuffers.get(actorName))
		{
			//Modify the boolean variable
			while(runtime.outputBufferState.get(actorName))
			{
				try
				{
					runtime.listOfOutputBuffers.get(actorName).wait();
					runtime.sendToDestinationNodes(runtime.listOfOutputBuffers.get(actorName).remove());
					runtime.outputBufferState.put(actorName, true);
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
}

