package com.applicationOwner;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import com.dataPacket.*;

public class ApplicationOwner {
	
	private Vector <String> listOfActors;
	private Vector <String> dataSources;
	private Vector <String> dataSinks;
	private Hashtable <String, Vector <String>> adjacencyList;
	private Hashtable <String, String> listOfRuntimes;
	private Hashtable <String, String> listOfActorCodeLocations;
	private Hashtable <String,String> listOfActorRuntimePairs;
	private Hashtable <String,String> dataSourceInfo;
	private Hashtable <String,String> dataSinkInfo;
	private Hashtable <String, Vector <String>> listOfActorsAssignedToRuntimes;
	private DatagramSocket socket;
	private final int PACKETSIZE = 5000;
	
	ApplicationOwner(int port)
	{
		adjacencyList = new Hashtable <String, Vector <String>>();
		listOfActors = new Vector<String>();
		listOfRuntimes = new Hashtable <String, String>();
		listOfActorCodeLocations = new Hashtable <String, String>();
		listOfActorRuntimePairs = new Hashtable <String, String>();
		dataSourceInfo = new Hashtable <String, String>();
		dataSinkInfo = new Hashtable <String, String>();
		listOfActorsAssignedToRuntimes = new Hashtable <String, Vector <String>> ();
		dataSources = new Vector <String>();
		dataSinks = new Vector <String>();
		
		try {
			socket = new DatagramSocket(port);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	
	/*
	 * Load the information pertaining to the different runtimes. Currently, a runtime identifier and 
	 * a port number for communication are stored.
	 */
	
	public void loadRuntimeInfo()
	{
		String fileLocation = "C:\\files\\runtimeInfo.txt";
		try
		{
			FileInputStream fstream = new FileInputStream(fileLocation);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String [] tokens;	
			
			//Read the graph from the file and create an adjacency list
			for(String readLine; (readLine = br.readLine()) != null; ) 
			{
				tokens = readLine.split(" ");
				if(!listOfRuntimes.containsKey(tokens[0]))
					listOfRuntimes.put(tokens[0], tokens[1]);
		    }
			
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/*
	 * Read the information of the data sources including their name and contact information
	 */
	public void loadDataSourcesInfo()
	{
		String fileLocation = "C:\\files\\dataSources.txt";
		try
		{
			FileInputStream fstream = new FileInputStream(fileLocation);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String [] tokens;	
			
			//Read the graph from the file and create an adjacency list
			for(String readLine; (readLine = br.readLine()) != null; ) 
			{
				tokens = readLine.split(" ");
				if(!dataSourceInfo.containsKey(tokens[0]))
					dataSourceInfo.put(tokens[0], tokens[1]);
		    }
			
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/*
	 * Read the information of the data sinks including their name and contact information
	 */
	public void loadDataSinksInfo()
	{
		String fileLocation = "C:\\files\\dataSinks.txt";
		try
		{
			FileInputStream fstream = new FileInputStream(fileLocation);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String [] tokens;	
			
			//Read the graph from the file and create an adjacency list
			for(String readLine; (readLine = br.readLine()) != null; ) 
			{
				tokens = readLine.split(" ");
				if(!dataSinkInfo.containsKey(tokens[0]))
					dataSinkInfo.put(tokens[0], tokens[1]);
		    }
			
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/*
	 * Retrieve the code locations of the actors
	 */
	public void getActorCodeLocations()
	{
		String fileLocation = "C:\\files\\actorCodeLocations.txt";
		try
		{
			FileInputStream fstream = new FileInputStream(fileLocation);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String [] tokens;	
			
			//Read the graph from the file and create an adjacency list
			for(String readLine; (readLine = br.readLine()) != null; ) 
			{
				tokens = readLine.split("@");
				
				if(!listOfActorCodeLocations.containsKey(tokens[0]))
					listOfActorCodeLocations.put(tokens[0], tokens[1]);
		    }
			
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/*
	 * This function allows the owner to select the nodes in the mobile cloud system where the application
	 * will be deployed. 
	 */
	public void assignActorsToRuntimes()
	{
		//Retrieve the list of available runtimes
		loadRuntimeInfo();
		
		//Retrieve the application structure using the adjacency list
		createGraphAdjacencyList();
		
		/*
		 * Send a copy of the adjacency list to all the runtimes as well as the data sources. This information will be used to handle the issue 
		 * of multiple actors within a single runtime
		 */
		
		Set set = listOfRuntimes.entrySet();
		Iterator iter = set.iterator();
		int currentPort = 0;
		InetAddress currentAddress = null;
		while(iter.hasNext())
		{
			Map.Entry <String, String> map = (Map.Entry <String, String> )iter.next();
			String addressKey = map.getValue();
			try
			{
				String addrInfo [] = addressKey.split("@");
				currentPort = Integer.parseInt(addrInfo[0]);
				currentAddress = InetAddress.getByName(addrInfo[1]);
				
				DataPacket packet = getDataPacket("", "Application graph");
				packet.additionalData = adjacencyList;
				sendMessage(packet, currentAddress, currentPort);
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		Set sourceInfo = dataSourceInfo.entrySet();
		Iterator iterator = sourceInfo.iterator();
		while(iterator.hasNext())
		{
			Map.Entry <String, String> map = (Map.Entry <String, String> )iterator.next();
			String addressKey = map.getValue();
			try
			{
				String addrInfo [] = addressKey.split("@");
				currentPort = Integer.parseInt(addrInfo[0]);
				currentAddress = InetAddress.getByName(addrInfo[1]);
				
				DataPacket packet = getDataPacket("", "Application graph");
				packet.additionalData = adjacencyList;
				sendMessage(packet, currentAddress, currentPort);
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		
		/*
		 * Identify the individual application nodes from the list and then assign them to the runtimes
		 * Also maintain a list of nodes assigned to runtimes
		 */
		
		//Fix this part later - The assignment of actors to runtimes
		Set s = listOfRuntimes.entrySet();
		Iterator it = s.iterator();
		
		int i = 0;
		while(it.hasNext())
		{
			Map.Entry <String, Integer> entry = (Map.Entry<String, Integer>)it.next();
			listOfActorRuntimePairs.put(listOfActors.get(i), entry.getKey());
			
			if(!listOfActorsAssignedToRuntimes.containsKey(entry.getKey()))
				listOfActorsAssignedToRuntimes.put(entry.getKey(), new Vector <String> ());
			
			listOfActorsAssignedToRuntimes.get(entry.getKey()).add(listOfActors.get(i));
			i++;
			if(i == listOfActors.size())
				break;
		}
		
			
	}
	
	/*
	 * This function uses the application connectivity graph to setup the connections between the different computing nodes
	 * 
	 */
	
	public void setupApplicationConnectivity()
	{
		//Use an adjacency list representation for a graph
		
		//Use the created adjacency list to setup the connectivity between the nodes
		
				
		Set s = adjacencyList.entrySet();
		Iterator i = s.iterator();
		
		String addressKey = "";
		int currentNodePort = 100;
		InetAddress currentNodeAddress = null;
		
		
		//Iterate over the list of nodes to setup the application connectivity
		while(i.hasNext())
		{
			Map.Entry <String, Vector <String>> record = (Map.Entry <String, Vector <String>>)i.next();
			
			//Get the first node
			String nodeName = record.getKey();
			addressKey = "";
			//Check if the node is a data source and get the contact information of the node
			if(nodeName.startsWith("source"))
			{
				dataSources.add(nodeName);
				addressKey = dataSourceInfo.get(nodeName);
			}
				
			else if(nodeName.equalsIgnoreCase("sink"))
			{
				dataSinks.add(nodeName);
				addressKey = dataSinkInfo.get(nodeName);
			}
				
			else
			{
				//If the node is an actor, then retrieve the address information of the runtime it is currently associated with
				
				String runtimeOfCurrentNode = listOfActorRuntimePairs.get(nodeName);
				addressKey = listOfRuntimes.get(runtimeOfCurrentNode);
			}
			
			//Retrieve the address and port number of the current node being examined
			
			try
			{
				String addrInfo [] = addressKey.split("@");
				currentNodePort = Integer.parseInt(addrInfo[0]);
				currentNodeAddress = InetAddress.getByName(addrInfo[1]);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			/*
			 * Once the contact information of the current node
			 * has been sorted out, start retrieving the adjacent nodes and the corresponding runtimes
			 */
								
			String listOfRuntimeAddresses = getListOfRuntimeAddressesOfAdjacentNodes(nodeName);
			
			/*
			 * Notify the current node about its destination nodes 
			 */
		
		//	sendMessage(listOfRuntimeAddresses, currentNodeAddress, currentNodePort);
			sendMessage(getDataPacket(listOfRuntimeAddresses, "DESTINATIONNODES"), currentNodeAddress, currentNodePort);
			
		}
		
	}
	
	/*
	 * Retrieving the runtimes corresponding to the adjacent nodes of a given actor node
	 */
	
	public String getListOfRuntimeAddressesOfAdjacentNodes(String currentActorNode)
	{
		Vector <String> runtimes = new Vector <String> ();
		
		//Retrieve the actors adjacent to the source and their corresponding runtimes
		for(String adjacentNode:adjacencyList.get(currentActorNode))
		{
			if(adjacentNode.startsWith("sink"))
			{
				if(!runtimes.contains(adjacentNode))
					runtimes.add(adjacentNode);
				continue;
			}
			
			String runtime = listOfActorRuntimePairs.get(adjacentNode);
			System.out.println("Node " + adjacentNode + " Runtime " + runtime);
			
			if(!runtimes.contains(runtime))
				runtimes.add(runtime);
		}
		
		
		//Retrieve the list of runtime addresses based on the runtime information from the previous list
		String listOfRuntimeAddresses = "";
		String runtimeAddress = "";
		
		for(String currentRuntime: runtimes)
		{
			if(currentRuntime.startsWith("sink"))
				runtimeAddress = dataSinkInfo.get(currentRuntime);
			else
				runtimeAddress = listOfRuntimes.get(currentRuntime);
				
				// Send a message to the destination runtimes informing them that the current node is a data sender
				
				String [] addrInfo = runtimeAddress.split("@");
				int port = 0;
				InetAddress address =  null;
				try {
					port = Integer.parseInt(addrInfo[0]);
					address = InetAddress.getByName(addrInfo[1]);
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				//Send a message denoting that the current runtime sends data to the destination
			
				if(!currentActorNode.startsWith("source"))
				{
					String sourceNodeAddress = listOfRuntimes.get(listOfActorRuntimePairs.get(currentActorNode));
					sendMessage(getDataPacket(sourceNodeAddress+"END", "SOURCENODES"), address, port);
				}
				else if(currentActorNode.startsWith("sink"))
					continue;
				else
				{
					String sourceNodeAddress = dataSourceInfo.get(currentActorNode);
					sendMessage(getDataPacket(sourceNodeAddress+"END", "SOURCENODES"), address, port);
				}
				
			//	sendMessage(message, address, port);
		
				
			
			listOfRuntimeAddresses = listOfRuntimeAddresses + runtimeAddress+"END";
			
			
			
		}
		
		return listOfRuntimeAddresses;
	}
	/*
	 * Generic function for sending messages to remote machines
	 */
	
	public void sendMessage(DataPacket dataPacket, InetAddress address, int port)
	{
		try
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.flush();
			oos.writeObject(dataPacket);
			byte[] data = baos.toByteArray();
			
			
			DatagramSocket socket = new DatagramSocket();
			DatagramPacket packet = new DatagramPacket( data, data.length, address, port );
			socket.send(packet);
			socket.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/*
	 * Read the graph adjacency list from a file and populate the adjacency list
	 */
	
	public void createGraphAdjacencyList()
	{
		String fileLocation = "C:\\files\\applicationGraph.txt";
		try
		{
			FileInputStream fstream = new FileInputStream(fileLocation);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String [] tokens;	
			
			//Read the graph from the file and create an adjacency list
			for(String readLine; (readLine = br.readLine()) != null; ) 
			{
				tokens = readLine.split(" ");
				
				if(!adjacencyList.containsKey(tokens[0]))
					adjacencyList.put(tokens[0], new Vector <String>());
				
				if(!(tokens[0].contains("source")|| tokens[0].contains("sink")))
					if(!listOfActors.contains(tokens[0]))
						listOfActors.add(tokens[0]);
				
				for(int i = 1; i < tokens.length;i++)
				{
				
					adjacencyList.get(tokens[0]).add(tokens[i]);
					
					
					if(!(tokens[i].contains("source")|| tokens[i].contains("sink")))
						if(!listOfActors.contains(tokens[i]))
							listOfActors.add(tokens[i]);

				}
				
									
		        // process the line.
		    }
			System.err.println(adjacencyList.toString());
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/*
	 * This function sends the list of actor-runtime pairings to the application coordinator for error correction purposes
	 */
	public void notifyCoordinator()
	{
		
		try {
			
			DataPacket packet = new DataPacket();
			packet.data = this.listOfActorRuntimePairs.toString();
			packet.message = "ActorRuntimePairs";
			packet.additionalData = this.listOfActorRuntimePairs;
			sendMessage(packet, InetAddress.getByName("localhost"), 7000 );
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	
	/*
	 * This function sends a signal to the different runtimes to fetch the actor codes from the code repositories
	 */
	public void signalRuntimesToFetchActorCodes()
	{
		//Retrieve the list of the runtimes that have been assigned to the actors
		
		Set s = listOfActorRuntimePairs.entrySet();
		Iterator i = s.iterator();
		int port;
		InetAddress address = null;
		
		
		while(i.hasNext())
		{
			String data = null;
			Map.Entry <String, String> actorRuntimePair = (Map.Entry <String, String>)i.next();
			String runtime = actorRuntimePair.getValue();
			String addressKey = listOfRuntimes.get(runtime);
			
			try
			{
				String [] addrInfo = addressKey.split("@");
				port = Integer.parseInt(addrInfo[0]);
				address = InetAddress.getByName(addrInfo[1]);

				
				//Retrieve the code location of the actor
				String actorName = actorRuntimePair.getKey();
				String codeLocation = listOfActorCodeLocations.get(actorName);
				
				if(codeLocation != null)
					data = codeLocation.trim();
				
				DataPacket packet = getDataPacket(data, "Fetch codes");
				packet.additionalData = actorName;
				sendMessage(packet, address, port);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}
		
	}
	
	/*
	 * Generic function for constructing a data packet
	 */
	public DataPacket getDataPacket(String data, String message)
	{
		DataPacket packet = new DataPacket();
		packet.data = data;
		packet.message = message;
		return packet;
	}
	
	/*
	 * Notify the data sources to start the transmission of the data
	 */
	public void signalDataSources()
	{
		String message = "Start transmission";
		int port;
		InetAddress address = null;
		
		for(String dataSource: dataSources)
		{
			String addressKey = dataSourceInfo.get(dataSource);
			
			String [] addrInfo = addressKey.split("@");
			port = Integer.parseInt(addrInfo[0]);
			try {
				address = InetAddress.getByName(addrInfo[1]);
				sendMessage(getDataPacket(message, message), address, port);
			} 
			
			catch (UnknownHostException e) {
				
				e.printStackTrace();
			}
		}
		
	}
	
	/*
	 * This function carries out the network reconnection functionality for a new runtime
	 */
	public void initiateReconnectionForNewRuntime(String actorName)
	{
		/*
		 * Step 1 - Repair the connections to those nodes that have the current actor as the source
		 * Use the adjacency list to determine the nodes adjacent to the runtime corresponding to this actor and repair the connections
		 */
		
		String runtimeOfCurrentNode = listOfActorRuntimePairs.get(actorName);
		String addressKey = listOfRuntimes.get(runtimeOfCurrentNode);
		
		
		
		String [] addrInfo = addressKey.split("@");
		int currentNodePort = Integer.parseInt(addrInfo[0]);
		InetAddress currentNodeAddress = null;
		try {
			currentNodeAddress = InetAddress.getByName(addrInfo[1]);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String listOfRuntimeAddresses = getListOfRuntimeAddressesOfAdjacentNodes(actorName);
		
		/*
		 * Notify the current node about its destination nodes 
		 */
	
		sendMessage(getDataPacket(listOfRuntimeAddresses, "DESTINATIONNODES"), currentNodeAddress, currentNodePort);
		
		/*
		 * Step 2 - Repair the connections for all the nodes that had the damaged node as a source
		 */
		
		Set s = adjacencyList.entrySet();
		Iterator i = s.iterator();
		String destinationAddressKey;
		boolean sourceNodeAffected = false;
		String dataSourceName = null;
		
		while(i.hasNext())
		{
			Map.Entry <String, Vector <String>> map = (Map.Entry <String, Vector <String>>)i.next();
			
			String currentActorName = map.getKey();
			Vector <String> adjacentActors = map.getValue();
			
			//If any of the current nodes have the damaged actor as the destination nodes, send a message to those nodes
			if(adjacentActors.contains(actorName))
			{
				if(currentActorName.startsWith("source"))
				{
					destinationAddressKey = dataSourceInfo.get(currentActorName);
					sourceNodeAffected = true;
					dataSourceName = currentActorName;

				}
				else
					destinationAddressKey = listOfRuntimes.get(listOfActorRuntimePairs.get(currentActorName));
				
				
				addrInfo = destinationAddressKey.split("@");
				currentNodePort = Integer.parseInt(addrInfo[0]);
				try {
					currentNodeAddress = InetAddress.getByName(addrInfo[1]);
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				sendMessage(getDataPacket(addressKey+"END", "DESTINATIONNODES"), currentNodeAddress, currentNodePort);
				
				
					
			}
	
		}
		
		/*
		 * Step 3 - Notify the data sources - if affected to resume sending data
		 */
		if(sourceNodeAffected)
		{
			String dataSourceaddressKey = dataSourceInfo.get(dataSourceName);
			addrInfo = dataSourceaddressKey.split("@");
			int dataSourcePort = Integer.parseInt(addrInfo[0]);
			InetAddress dataSourceAddress = null;
			try {
				dataSourceAddress = InetAddress.getByName(addrInfo[1]);
				sendMessage(getDataPacket(addressKey, "Repair"), dataSourceAddress, dataSourcePort);
			} 
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}
	}
	/*
	 * The application owner listens here for messages 
	 */
	public void waitForMessage()
	{
		byte[] recvBuf = new byte[PACKETSIZE];
		DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
	  	
		while(true)
	  	{
	  		try
	  		{
	  			socket.receive(packet);
	  			
	  			//Receive and deserialize the packet
	  			ByteArrayInputStream byteStream = new ByteArrayInputStream(recvBuf);
				ObjectInputStream is = new ObjectInputStream(new BufferedInputStream(byteStream));
	  			DataPacket dataPacket = (DataPacket)is.readObject();
	  			String data = dataPacket.data.trim();
	  			String message = dataPacket.message.trim();
	  			
	  			if(message.startsWith("Runtime"))
	  			{
	  				//Remove inactive runtime
	  				
	  				String inactiveRuntime = listOfActorRuntimePairs.get(data);
	  				listOfRuntimes.remove(inactiveRuntime);
	  				
	  				//New runtime actor pairing
	  				listOfActorRuntimePairs.put(data, message);
		  				
	  			}
	  			
	  			if(message.equalsIgnoreCase("Initiate reconnection"))
	  				initiateReconnectionForNewRuntime(data);
	  		}
	  		catch(Exception e)
	  		{
	  			e.printStackTrace();
	  		}
	  		
	  	}
	}
	
	public static void main(String args[])
	{
		ApplicationOwner appOwner = new ApplicationOwner(3500);
		
		appOwner.loadRuntimeInfo();
		
		appOwner.getActorCodeLocations();
		appOwner.loadDataSourcesInfo();
		appOwner.loadDataSinksInfo();
		
		appOwner.assignActorsToRuntimes();
		appOwner.notifyCoordinator();
		appOwner.setupApplicationConnectivity();
	//	appOwner.notifyRuntimesAboutSources();
		
		appOwner.signalRuntimesToFetchActorCodes();
		appOwner.signalDataSources();
		
		appOwner.waitForMessage();
	}
}
