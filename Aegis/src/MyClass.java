public class MyClass {
	
	public void sayHello() {
		System.out.println("Hello world from the loaded class !!!");
	}
	
	public void test() {
		System.out.println("Hello test world from the loaded class !!!");
	}
	
	public static void main(String args[])
	{
		System.out.println("Hello from jar" + args[0]);
	}

}