import java.lang.reflect.Method;

public class ActorCommunicationManager implements Runnable{
	
	private Class actorClass;
	private Object actorInstance;
	private Runtime runtime;
	
	ActorCommunicationManager(Class actorClass, Object actorInstance, Runtime runtime)
	{
		this.actorClass = actorClass;
		this.actorInstance = actorInstance;
		this.runtime = runtime;
	}
	
	public void run()
	{
		try
		{
			Method actorMethod = actorClass.getMethod("run");
			actorMethod.invoke(actorInstance);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	

}
